#!/bin/bash

VENV=/usr/local/venv/backend

sudo apt-get update
sudo apt-get install -y python3-venv unbound

test -d $VENV || sudo python3 -m venv $VENV
sudo $VENV/bin/pip install --upgrade pip
sudo $VENV/bin/pip install wheel
sudo $VENV/bin/pip install --requirement /vagrant/api/requirements.in

cat << EOF | sudo tee /etc/systemd/system/api-backend.service
[Unit]
Description=Backend
WantedBy=multi-user.target

[Service]
Type=simple
WorkingDirectory=/vagrant/api
ExecStart=$VENV/bin/uvicorn main:app --host 0.0.0.0 --port 8000

[Install]
WantedBy=multi-user.target
EOF

cat << EOF | sudo tee /etc/systemd/system/api-frontend.service
[Unit]
Description=Frontend
WantedBy=multi-user.target

[Service]
Type=simple
WorkingDirectory=/vagrant/front
ExecStart=/usr/bin/python3 -m http.server 80

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable api-frontend.service api-backend.service
sudo systemctl start api-frontend.service api-backend.service

cat << EOF | sudo tee /etc/unbound/unbound.conf.d/90-config.conf
server:
    interface: 0.0.0.0
    access-control: 0.0.0.0/0 allow

    local-zone: "exemple.net." transparent
    local-data: "api.exemple.net 3600 IN A 192.168.125.11"
    local-data: "api.exemple.net 3600 IN A 192.168.125.12"
    local-data: "www.exemple.net 3600 IN A 192.168.125.11"
    local-data: "www.exemple.net 3600 IN A 192.168.125.12"

forward-zone:
    name: "."
    forward-addr: 1.1.1.1
EOF

sudo systemctl restart unbound.service
