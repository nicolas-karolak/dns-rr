let http = require('http')
function sequentialRequest(count){
  if (count == undefined) {count = 0}
  count++;
  if (count > 10000) {return}
    http.get("http://api.exemple.net:8000", (res) => {
        let data = "";
        res.on("data", d => {
            data += d
        });
        res.on("end", () => {
          console.log(count, data);
          setTimeout(
              () => sequentialRequest(count),
              1000
          );
        });
    }).end();
}
sequentialRequest();
