package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func request(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	return string(body)
}

func main() {
	i := 1
	for {
		data := request("http://api.exemple.net:8000")
		log.Println(i, data)
		i += 1
		time.Sleep(1 * time.Second)
	}
}
