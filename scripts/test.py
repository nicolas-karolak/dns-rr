#!/usr/bin/python3

from http.client import HTTPConnection
import json
import requests
from time import sleep


def std_request(host: str, port: int) -> dict:
    conn = HTTPConnection(host, port)
    conn.request('GET', '/')
    resp = conn.getresponse()
    data = json.loads(resp.read().decode())
    conn.close()

    return data


def lib_request(host: str, port: int) -> dict:
    response = requests.get(f'http://{host}:{port}')
    data = response.json()

    return data


def main() -> None:
    i = 0
    while True:
        i += 1
        response = lib_request('api.exemple.net', 8000)
        print(i, response)
        sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit(0)
