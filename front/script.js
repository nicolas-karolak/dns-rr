const app = new Vue({
    el: '#app',
    data: {
        host: "",
        data: "",
    },
    methods: {
        fetchAPIData() {
			fetch("http://api.exemple.net:8000/", {
				"method": "GET"
			})
			.then(response => {
				if(response.ok){
					return response.json()
				} else{
					alert("Server returned " + response.status + " : " + response.statusText);
				}
			})
			.then(response => {
				this.host = response.host;
				this.data = response.data;
				console.log(response);
			})
			.catch(err => {
				console.log(err);
			});
        }
    }
})
