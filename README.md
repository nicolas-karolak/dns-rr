Conclusion pour le DNS RR :

- Firefox :
- Chrome : OK mais au bout de ~60 secondes
- Python (lib standard `http.client`) : OK ~60 secondes
- Python (lib externe `requests`) : OK ~60 secondes
- Go (`net/http`) : OK en quelques secondes
- Node.js : **KO** ne gère pas du tout le DNS RR, il prend uniquement la première réponse (même si injoignable)
